const fs = require('fs');
const path = require('path');
const request = require('request');
const { exec } = require('child_process');

function burnAudioToVideo(videoPath, audioPath, callback) {
  const fileExtension = videoPath.split('.').pop();
  const targetPath = path.join('tmp', `burned-video-audio-${Date.now()}.${fileExtension}`);

  exec(`ffmpeg -i ${videoPath} -i ${audioPath} -map "0:v?" -map "1:a?" -c:v copy ${targetPath}`, (err) => {
    if (err) return callback(err);
    console.log('done burning')
    return callback(null, targetPath)
  })
}

function convertToMp3(filePath, callback) {
  const targetPath = path.join('tmp', `mp3-audio-${Date.now()}.mp3`);

  exec(`ffmpeg -i ${filePath} ${targetPath}`, (err) => {
    if (err) return callback(err);
    return callback(null, targetPath)
  })
}

function getRemoteFile(url, callback) {
  const filePath = path.join('tmp', `file-${parseInt(Date.now() + Math.random() * 1000000) + "." + url.split('.').pop()}`);
  request
    .get(url)
    .on('error', (err) => {
      callback(err)
    })
    .pipe(fs.createWriteStream(filePath))
    .on('error', (err) => {
      callback(err)
    })
    .on('finish', () => {
      callback(null, filePath)
    })
}

module.exports = {
    getRemoteFile,
    burnAudioToVideo,
    convertToMp3
}