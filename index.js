const AUDIO_PROCESSOR_API_ROOT = process.env.AUDIO_PROCESSOR_API_ROOT

const fs = require('fs');

const superagent = require('superagent');
const amqp = require('amqplib/callback_api');

const storageService = require('./storage');
const videowikiGenerators = require('@videowiki/generators');
const utils = require('./utils');

const PROCESS_NOISECANCELLATIONVIDEO_AUDIO_QUEUE = `PROCESS_NOISECANCELLATIONVIDEO_AUDIO_QUEUE`;
const PROCESS_NOISECANCELLATIONVIDEO_AUDIO_FINISHED_QUEUE = `PROCESS_NOISECANCELLATIONVIDEO_AUDIO_FINISHED_QUEUE`;

const VIDEO_NOSIECANCELLATION_DIRECTORY = 'video_noisecancellation'

let channel;

// Create necessary file dirs 
const APP_DIRS = ['./tmp'];
APP_DIRS.forEach(dir => {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
})

amqp.connect(process.env.RABBITMQ_SERVER, (err, conn) => {
    if (err) {
        console.log('error is', err);
    }

    conn.createChannel((err, ch) => {
        if (err) {
            console.log('Error creating conection ', err);
            return process.exit(1);
        }
        console.log('connection created')
        channel = ch;

        channel.on('error', (err) => {
            console.log('RABBITMQ ERROR', err)
            process.exit(1);
        })
        channel.on('close', () => {
            console.log('RABBITMQ CLOSE')
            process.exit(1);
        })
        channel.prefetch(1);
        channel.assertQueue(PROCESS_NOISECANCELLATIONVIDEO_AUDIO_QUEUE, { durable: true });
        channel.assertQueue(PROCESS_NOISECANCELLATIONVIDEO_AUDIO_FINISHED_QUEUE, { durable: true });

        channel.consume(PROCESS_NOISECANCELLATIONVIDEO_AUDIO_QUEUE, processNoiseCancellationVideoCallback, { noAck: false });

        const { app, server } = videowikiGenerators.serverGenerator({ uploadLimit: 50 });

        videowikiGenerators.healthcheckRouteGenerator({ router: app, rabbitmqConnection: channel.connection });
        server.listen(4000);
    })
})



function processNoiseCancellationVideoCallback(msg) {
    const { id, url, title } = JSON.parse(msg.content.toString());
    processNoiseCancellationVideo({ url, title }, (err, res) => {
        channel.ack(msg);
        if (err) {
            console.log('error', err);
            channel.sendToQueue(PROCESS_NOISECANCELLATIONVIDEO_AUDIO_FINISHED_QUEUE, new Buffer(JSON.stringify({ id, status: 'failed' })));
        } else {
            const { url, Key } = res;
            channel.sendToQueue(PROCESS_NOISECANCELLATIONVIDEO_AUDIO_FINISHED_QUEUE, new Buffer(JSON.stringify({ id, url, Key, status: 'done' })));
        }
    })
}

function processNoiseCancellationVideo({ url, title }, callback = () => { }) {
    console.log('Starting for noise cancellation file', title, url);
    utils.getRemoteFile(url, (err, videoPath) => {
        if (err) return callback(err);
        console.log('downloaded file', videoPath)
        utils.convertToMp3(videoPath, (err, mp3Path) => {
            if (err) return callback(err);
            processAudio({ filePath: mp3Path, outputFormat: 'mp3' }, (err, processedAudioPath) => {
                // Burn the new audio to the video
                fs.unlink(mp3Path, () => {});
                if (err) return callback(err);
                utils.burnAudioToVideo(videoPath, processedAudioPath, (err, finalFilePath) => {
                    if (err) return callback(err);
                    fs.unlink(processedAudioPath, () => { })
                    fs.unlink(videoPath, () => { })
                    // Upload the burned video
                    const fileName = `${title}-${Date.now()}.${finalFilePath.split('.').pop()}`;
                    storageService.saveFile(VIDEO_NOSIECANCELLATION_DIRECTORY, fileName, fs.createReadStream(finalFilePath))
                        .then((uploadRes) => {
                            fs.unlink(finalFilePath, () => { });
                            return callback(null, uploadRes);
                        }).catch(callback);
                })

            })
        })
    })
}

function processAudio({ filePath, outputFormat }, callback = () => { }) {
    superagent.post(`${AUDIO_PROCESSOR_API_ROOT}/process_audio`)
        .field('outputFormat', outputFormat)
        .attach('file', fs.createReadStream(filePath))
        .then((res) => {
            const audioFileName = `cleared_audio_${Date.now()}.mp3`;
            fs.writeFile(audioFileName, res.body, (err) => {
                if (err) return callback(err);
                return callback(null, audioFileName);
            })
        })
        .catch(err => {
            return callback(err);
        })
}