const STORAGE_SERVICE_API_ROOT = process.env.STORAGE_SERVICE_API_ROOT;
const superagent = require('superagent');

function saveFile(directoryName, fileName, fileStream) {
    return new Promise((resolve, reject) => {
        superagent.post(STORAGE_SERVICE_API_ROOT)
        .field('directoryName', directoryName)
        .field('fileName', fileName)
        .attach('file', fileStream)
        .then((res) => {
            resolve(res.body);
        })
        .catch(err => {
            console.log('error is', err)
            reject(err);
        })
    })
}

function deleteFile(directoryName, fileName) {
    return new Promise((resolve, reject) => {
        let Key = fileName ? `${directoryName}/${fileName}` : directoryName;
        superagent.delete(`${STORAGE_SERVICE_API_ROOT}/${Key}`)
        .then((res) => {
            console.log('res is', res)
            resolve(res.body);
        })
        .catch(err => {
            console.log('error is', err)
            reject(err);
        })
    })
}

module.exports = {
    saveFile,
    deleteFile,
}